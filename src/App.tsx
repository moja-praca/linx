import React, { FC, useCallback } from 'react';
import styled from 'styled-components';
import Wall from './img/wallrr.png'
import Avatar from './components/Avatar';
import Navigation from './components/Navigation';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Skills from './components/Sklills';
import Info from './components/Info';
import { fighters } from './data';



const MainApp = styled('div')`
  background-image: url(${Wall});
  background-repeat: no-repeat;
  background-color: black;
  background-position: center;
  text-align: center;
  width: 100%;
  height: 100vh;
  
`

const Body = styled('body')`
  width: 900px;
  margin: 0 auto;
  height: 800px;
  display: flex;
  flex-direction: column;
  align-items: start;
  justify-content:start;
`

const Browse = styled('div')`
width: 60%;
height: 50%;
margin: 0 auto;
margin-top: 100px;
border-radius: 20px;
animation: rotate 5s ease-in-out infinite;
display: flex;
flex-direction: row;
justify-content: space-between;
padding: 0 60px 0 60px;
align-items: center;
background-color: #8080808f;
transition: all 300ms ease-in-out;

@keyframes rotate {
  0% {
    border: solid 2px blue;
  }
  50% {
    border: solid 2px purple;
  }
  100% {
    border: solid 2px blue;
  }

}

`
const Select = styled('select')`
  width: 200px;
  height: 40px;
  margin-top: 20px;
  padding-right: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  border: none;
  border-radius: 18px;
  color: white;
  background-color: purple;
  margin-left: 110px;
`
type MyType = {
  fighters: Fight[];
}
const App:FC<MyType> = ({fighters}) => {

  const handleConsole = useCallback(() => {
  
  },[])

  return (
    <MainApp>
      <header >
     
      </header>
      <Body>
        <BrowserRouter>
        <Select name="" id="">
          <option value="Warior">Warrior</option>
          <option value="Rocket">Rocket</option>
        </Select>
        <Browse>
          <div>
            <Avatar/>
            <Navigation />
            <button onClick={handleConsole}>klik</button>
          </div>
          <div>
              <Routes>
                <Route path='/' Component={Skills} />
                <Route path='/Info' Component={Info}  />
            </Routes>
          </div>
        </Browse>
       
      
        </BrowserRouter>
        
      </Body>
    </MainApp>
  );
}

export default App;
