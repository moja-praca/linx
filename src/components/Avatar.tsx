import React from "react";
import styled from "styled-components";
import Panda from '../img/panda.jpg'

const ImageDiv = styled('div')`
    height: 200px;
    width: 200px;
    overflow: hidden;
    border-radius: 50%;
    border: solid 2px #fcdd56;
    display: flex;
    justify-content: center;
    align-items: center;
    

`

const AvatarImg = styled('img')`
    height: 100%;
`

const Avatar = () => {
    return(
        <ImageDiv>
            <AvatarImg src={Panda} alt="Avatar" />

        </ImageDiv>
    )
};


export default Avatar