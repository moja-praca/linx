import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const Nav = styled('nav')`
    display: flex;
    flex-direction: column;
`

const MyLink = styled(Link)`
    font-size: 36px;
    color: white;
    text-decoration: none;
    margin: 10px 0 10px 0;
    border: 1px white solid;
    border-radius: 15px;
    transition: all 300ms ease-in-out;
    &:hover {
        transform: scale(1.05);
        box-shadow: 0px 0px 10px black;
    }
    &:focus {
        background-color: #a5bd4f;
    }
`

const Navigation = () => {
    return(
        <div>
            <Nav>
                <MyLink to='/' >Skills</MyLink>
                <MyLink to='/Info' >Info</MyLink>
            </Nav>

        </div>
    )
};

export default Navigation;