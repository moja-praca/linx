type Fight = {
    id: number;
    name: string;
    skills: string;
    image: string;
}